#include <iostream>
#include <fstream>
#include "pz05_main.h"
#include "../catch.hpp"


int Compare(std::string in, std::string out){
    std::string path = "/home/user/timp-v2/pz03/sourse/";
    std::ifstream ifl(path + in);
    std::ifstream ofl(path + out);
    std::string sin = "";
    std::string sout = "";

    for (int i = 0; ifl.eof() != true; i++)
        sin += ifl.get();

    for (int i = 0; ofl.eof() != true; i++)
        sout += ofl.get();

    ifl.close();
    ofl.close();

    /*std::cout << "first: " << sin << '\n';
    std::cout << "second: " << sout << '\n';*/

    if (sin == sout){
        std::cout << "Files are same\n";
        return 0;
    }else if ((sin != sout) && (sin != "") && (sout != "")){
        std::cout << "Files aren`t same\n";
        return -3;
    }else if ((sin == "") || (sout == "")){
        std::cout << "Atleast one file is \n";
        return -5;
    }else{
        std::cout << "Error\n";
        return -10;
    }
}