#include <iostream>
#include <cstdio>

#include "pz05_main.h"
#include "../pz03/pz03.h"
#define CATCH_CONFIG_MAIN
#include "../catch.hpp"


TEST_CASE ("c_valid_files", "[copy_tests]"){
    remove("../pz03/sourse/out.txt");
    int is_valid = Copy("in.txt", "out.txt");
    REQUIRE(is_valid != -1);
}
TEST_CASE ("c_same_files", "[copy_tests]"){
    remove("../pz03/sourse/out.txt");
    int is_valid = Copy("in.txt", "out.txt");
    REQUIRE(is_valid != -1);
    int are_same = Compare("in.txt", "out.txt");
    REQUIRE(are_same == 0);
}
TEST_CASE ("c_dif_files", "[copy_tests]"){
    remove("../pz03/sourse/out.txt");
    int is_valid = Copy("in.txt", "out.txt");
    REQUIRE(is_valid != -1);
    int are_same = Compare("in.txt", "out.txt");
    REQUIRE(are_same != 0);

}


TEST_CASE ("s_valid_files", "[sum_tests]"){
    int is_valid = Sum("i2.txt");
    REQUIRE(is_valid != -1);

}
TEST_CASE ("s_correct_values", "[sum_tests]"){
    int is_correct = Sum("i2.txt");
    REQUIRE(is_correct != -1);
    REQUIRE(is_correct == -13);
}
TEST_CASE ("s_incorrect_values", "[sum_tests]"){
    int is_incorrect = Sum("i2.txt");
    REQUIRE(is_incorrect != -1);
    REQUIRE(is_incorrect != -13);
}


TEST_CASE ("cr_valid_files", "[crypt_tests]"){
    remove("../pz03/sourse/out.txt");
    int is_valid = Crypt("in.txt", "out.txt", "zxc");
    REQUIRE(is_valid != -1);
}
TEST_CASE ("cr_not_empty_files", "[crypt_tests]"){
    remove("../pz03/sourse/out.txt");
    int is_empty= Crypt("in.txt", "out.txt", "zxc");
    REQUIRE(is_empty != -1);
    REQUIRE(is_empty != -666);
}
TEST_CASE ("cr_correct_xor", "[crypt_tests]"){
    remove("../pz03/sourse/out.txt");
    int is_correct = Crypt("in.txt", "out.txt", "zxc");
    REQUIRE(is_correct != -1);
    REQUIRE(is_correct != -376);
}