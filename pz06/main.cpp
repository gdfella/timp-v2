#include <iostream>
#include <SFML/Graphics.hpp>
#include <ctime>
#include <string>
#include <functional>

using namespace std;
using namespace sf;

void OpenWindow(RenderWindow &oldwin) {

    oldwin.setActive(false);
    //Get date
    time_t rawtime;
    struct tm* timeinfo;
    char buffer[80];
    time(&rawtime);
    timeinfo = localtime(&rawtime);
    strftime(buffer, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
    std::string str(buffer);
    string newTitle = "2020-3-07-kos" + str.substr(0, str.find(" "));
    
    RenderWindow window(VideoMode(535, 693), newTitle);
    while (window.isOpen()) {
        Event event;
        while (window.pollEvent(event)) {
            if (event.type == Event::Closed) {
                window.close();
                break;
            }
        }
    }
    //oldwin.setActive(true);
}

int main()
{
    //Setup window
    RenderWindow window(VideoMode(535, 693), "Awesome Notepad");
    Font font;
    font.loadFromFile("C:/Users/Danil/Documents/fonts/Press_Start_2P/PressStart2P-Regular.ttf");
    //Setup text sourse
    Text Text;
    Text.setFont(font);
    Text.setCharacterSize(24);
    Text.setFillColor(Color::Green);
    Text.setPosition(10, 10);
    String input;
    string newTitle;

    //Main loop
    while (window.isOpen()) {
        Event event;
        while (window.pollEvent(event)) {
            //Close win
            if (event.type == Event::Closed) {
                window.close();
                break;
            }
            //Type text
            if (event.type == Event::TextEntered) {
                if (event.key.code != Keyboard::BackSpace)
                    input.insert(input.getSize(), event.text.unicode);
                if (Keyboard::isKeyPressed(Keyboard::Enter)) {
                    string content = Text.getString().toAnsiString();
                    //Get 'window' command
                    if (content.find("window") != string::npos) {
                        Thread secondThread(OpenWindow, std::ref(window));
                        secondThread.launch();
                        secondThread.wait();
                        
                    }
                    else {
                        input.insert(input.getSize(), '\n');
                    }
                }
            }
            //Delete symbol
            if (event.key.code == Keyboard::BackSpace) {
                if (input.getSize() > 0)
                    input.erase(input.getSize() - 1, 1);
            }
        }
        //Render win
        Text.setString(input.getData());
        window.clear();
        window.draw(Text);
        window.display();
    }
}
