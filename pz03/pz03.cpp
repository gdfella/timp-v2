#include "pz03.h"
#include <sstream>
#include <algorithm>
#include <fstream>
#include <iostream>
using namespace std;

int Copy(string input, string output) {
    string path = "pz03/sourse/";
    ifstream ifl(path + input);
    ofstream ofl(path + output);
    string line = "";
    int c = 0;
    if(!ifl) {
        cout << "Can`t open file";
        return -1;
    }
    while (getline(ifl, line)) {
        ++c;
        ofl << line;
        ofl << '\n';
    }
    ifl.close();
    ofl.close();
    return c;
}


double Sum(string input) {
    string path = "pz03/sourse/";
    fstream fs;
    string line;
    string st = "";
    double s = 0;

    fs.open(path + input, ios::in);

    if(!fs) {
        cout << "Can`t open file";
        return -1;
    }

    while (getline(fs, line)) {
        replace(line.begin(), line.end(), ',', '.');
        st += line;
        st += "\n";
    }

    fs.close();
    fs.open(path + input, ios::out);
    fs << st;
    st = "";
    fs.close();

    fs.open(path + input, ios::in);
    while (!fs.eof()) {
        getline(fs, line);
        replace(line.begin(), line.end(), ' ', '\n');
        st += line;
        st += "\n";
    }
    fs.close();

    fs.open(path + input, ios::out | ofstream::trunc);
    fs << st;
    fs.close();

    fs.open(path + input);
    while (getline(fs, line))
        try { s += stod(line); }
        catch (const std::exception&) {
            cout << "Found not double element\n";
            return -13;
        }
    fs.close();
    return s;
}


int Crypt(string input, string output, string key) {
    string path = "pz03/sourse/";
    ifstream ifl(path + input);
    ofstream ofl(path + output);
    string sin = "";

    if(!ifl) {
        cout << "Can`t open file";
        return -27;
    }

    for (int i = 0; ifl.eof() != true; i++)
        sin += ifl.get();

    if(sin == ""){
        cout << "File is empty\n";
        return -666;
    }

    for (int i = 0; i < sin.size(); ++i)
        try {
            sin[i] ^= key[i%key.size()];
        }catch (const std::exception&) {
            cout << "XOR error\n";
            return -376;
        }



    ofl << sin;
    return ofl.tellp();

}
