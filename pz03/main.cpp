#include <iostream>
#include <sstream>
#include "pz03.h"
using namespace std;

int main()
{
    char f;
    string first, second, key;
    int b, c;
    double s;
    while(true){
        cin >> f;
        switch (f)
        {
            case 'q':
                return 0;
            case '1':
                cout << "Enter input filename: ";
                cin.ignore();
                getline(cin, first);
                cout << "Enter output filename: ";
                getline(cin, second);
                c = Copy(first, second);
                cout << c << " lines read...\n";
                break;

            case '2':
                cout << "Enter input filename: ";
                cin.ignore();
                getline(cin, first);
                s = Sum(first);
                cout << "Sum equal... " << s;
                break;

            case '3':
                cout << "Enter input filename: ";
                cin.ignore();
                getline(cin, first);
                cout << "Enter output filename: ";
                getline(cin, second);
                cout << "Enter key: ";
                getline(cin, key);
                b = Crypt(first, second, key);
                cout << "Amounts of encrypted bytes is... " << b << "bytes";
                break;

            default:
                cout << "Invalid input! Try again\n";
                break;
        }
    }
}
